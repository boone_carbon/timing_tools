#ifndef BOONES_HANDY_THROWN_TOGETHER_PROFILER_H
#define BOONES_HANDY_THROWN_TOGETHER_PROFILER_H

#include <sys/time.h>
#include <ros/console.h>
#include <ros/ros.h>


//!
//! Like hitting "start" on a stopwatch, this starts timing.
//!
//! Useful at the start of a loop
//!
#define TIME_START                              \
  struct timespec t_start, t_last, t_current;   \
  clock_gettime(CLOCK_REALTIME, &t_start);      \
  t_last = t_start;                             \
  t_current = t_start;

//!
//! Like hitting "lap" on a stopwatch, this logs time since last TIME_START or
//! TIME_LAP.
//!
//! @param tag [string] Prefixes the time in the log message. Should summarize the code
//! run since last call to TIME_START or TIME_LAP (e.g. the function name)
//!
#define TIME_LAP(tag)                                                   \
  t_last = t_current;                                                   \
  clock_gettime(CLOCK_REALTIME, &t_current);                            \
  ROS_INFO_STREAM_NAMED("profiling", tag << subtract(t_last, t_current)); \

//!
//! Like hitting "stop" on a stopwatch, this logs time since time TIME_START.
//!
//! Useful at the start of a loop.
//!
#define TIME_STOP                                            \
  clock_gettime(CLOCK_REALTIME, &t_current);                  \
  ROS_INFO_STREAM_NAMED("profiling", "Total" << subtract(t_start, t_current));

inline timespec subtract(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

inline std::ostream& operator <<(std::ostream& os, const timespec& t)
{
    os << "\t" << t.tv_sec*1.e3 + t.tv_nsec/1.e6 << " msec";
    return os;
}

#endif  // BOONES_HANDY_THROWN_TOGETHER_PROFILER_H
