# Simple profiling tool
I threw this together to make it a little easier to profile using clock_getting()

## Setup

    cd <project>/include
    ln -s <this_repo>/profiler.hpp  # Symbolically link to this file

## Adding to code
  
    #include "profiler.hpp"

1. Run TIME_START once at the beginning of the loop
2. Run TIME_LAP(tag) *after* each code snippet you want to time
3. Run TIME_STOP at the end of each loop to get total loop time

## Generating a report
0. Make sure roslaunch prints node output to the screen (if applicable)
1. Run the node with ROSCONSOLE='[${file}:${line}@${time}]: ${message}'
2. Pipe the roslog to a .log file
